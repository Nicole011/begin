const os = require('os');
const fs = require('fs');
// const im = require('basico/modules/imp.js');

//Leer archivo
// fs.readFile('data.txt', 'utf-8', (error, data) => {
//     if(error){
//         console.log(`Error: ${error}`);
//     }else{
//         console.log(data);
//     }
// })

//Proceso sincrono y Asincrono
/*Si tienes dos proceso A y B, A dura 10seg y B dura 5seg
    Para ejecutar B A debe finalizar, tiempo total de ejecucion 15seg(sincrono)

En asincrono tanto A como B se ejecutan al mismo tiempo

Evaluar condicion de ejecucion si se necesita sincrono o asincrono, Node.JS trabaja en asincrono por defecto
*/
/**********ASINCRONO**********/
// console.log('Iniciado');

// fs.readFile('data.txt', 'utf-8', (error, data) => {
//     if(error){
//         console.log(`Error: ${error}`);
//     }else{
//         console.log(data);
//     }
// })

// console.log('Finalizado');
/*Salida

Iniciado
Finalizado
Texto
*/

/************SINCRONO**********/
// console.log('Iniciado');

// let data = fs.readFileSync('data.txt', 'utf-8');
// console.log(data);

// console.log('Finalizado');
/*Salida
Iniciado
Texto
Finalizado
*/


//Operando el archivo

//Forma asincrona, se tiene una funcion de retorno
// fs.rename('data_s.txt', 'data.txt', (error) => {
//     if(error) throw error;
//     console.log('renombrado');
// })


//Agregar texto a un archivo
// fs.appendFile('data.txt', '\nEste texto se agrego', (error) => {
//     if(error) throw error;
// })


//Eliminar un archivo
// fs.unlink('data2.txt', (error) => {
//     if(error) throw error;
// })


//Copiar un archivo
//fs.createReadStream('data.txt').pipe(fs.createWriteStream('data3.txt'));

//console.log('inicio');
//Leer directorio de forma sincrona
// fs.readdir('./../../basico/', (error, files)=>{
//     files.forEach(file=>{
//         console.log(file);
//     })
// })

//Leer directorio de forma asincrona
// fs.readdirSync('./../../basico/').forEach(file=>{
//     console.log(file);
// })
//console.log('fin');

// let cpu = os.cpus();
// let sistema = os.platform();
// let user = os.hostname();
// let cpu_string = JSON.stringify(cpu);
// let result = im.sumar(10,5);
// let result2 = im.restar(10);

//console.log(user);
//console.log(cpu);
//console.log(im.subscriptores);
// console.log(result);
// console.log(result2);
// console.log(im.dos);

// im.saludar;
// im.saludar2;

// setTimeout(()=>{
//     console.log("Termine")
// }, 2000);

// fs.appendFile('test.txt', `informacion del cpu: ${cpu_string}`, function(error){
//     if(error){ 
//         console.log('Error al crear el archivo');
//     }
// })