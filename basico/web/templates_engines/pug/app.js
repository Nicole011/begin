const express = require('express');
const app = express();

let personas = [
    {
        id: 1,
        nombre: "Nicole"
    },
    {
        id: 2,
        nombre: "Pedro"
    },
    {
        id: 3,
        nombre: "Ana"
    }
]
app.set('view engine', 'pug');

app.get('/', (req, res)=>{
    res.render('template', {titulo:'test', mensaje:'Las personas', personas : personas});
});

app.listen(3000, ()=>{
    console.log('Corriendo en el puerto 3000');
});