// console.log('Este es un archivo externo');

let subscriptores = 25;

module.exports.subscriptores = subscriptores;


// function saludar2() {
//     console.log('hello');
// }

module.exports.saludar = function() {
    console.log('hello');
};

// module.exports.saludar2 = saludar2();


module.exports = {
    uno : () => {
        console.log('primero');
    },
    dos : subscriptores,
    sumar : (a,b) => a + b,
    // {
    //     return a + b;
    // } 
    restar : a => a - 1
}