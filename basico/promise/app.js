let prom = require('./promesa');

prom.calcular(2,3).then((resultado)=>{
    console.log(resultado);
}, (error)=>{ //Manejo de excepciones
    console.log(error); 
});

// let promesa = new Promise((resolve, reject)=>{
//     //resolve('Exito al procesar los datos');
//     reject('Error');
// });

// promesa.then((resultado)=>{
//     console.log(resultado);
// }, (error) =>{
//     console.log(error);
// });


